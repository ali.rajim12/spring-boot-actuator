package com.rajim.springbootactuatordemo.custom;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

/**
 * We can also create custom health indicator by implementing HealthIndicator  interface
 * or by extending AbstractHealthIndicator
 */
@Component
public class CustomHealthIndicator extends AbstractHealthIndicator {

	@Override
	protected void doHealthCheck(Health.Builder builder) throws Exception {
		// Use the builder to build the health status details that should be reported.
		// If you throw an exception, the status will be DOWN with the exception message.

		builder.up()
				.withDetail("app", "Alive and doing awesome ")
				.withDetail("error", "Nothing! I'm good.");
	}
}
